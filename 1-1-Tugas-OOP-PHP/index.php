<?php
    trait Hewan{
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;

        public function atraksi(){
            echo "{$this->nama} sedang {$this->keahlian} <br>";
        }
    }

    abstract class Fight{
        use Hewan;
        public $attackPower;
        public $defencePower;

        public function serang($hewan){
            echo "{$this->nama} sedang menyerang {$hewan->nama} <br>";

            $hewan->diserang($this);
        }

        public function diserang($hewan)
        {
            echo "{$this->nama} sedang diserang {$hewan->nama} <br>";

            $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
        }

        protected function getInfo(){
            echo "<br>";
            echo "Nama: {$this->nama}";
            echo "<br>";
            echo "Jumlah Kaki: {$this->jumlahKaki}";
            echo "<br>";
            echo "Keahlian: {$this->keahlian}";
            echo "<br>";
            echo "Darah: {$this->darah}";
            echo "<br>";
            echo "attackPower: {$this->attackPower}";
            echo "<br>";
            echo "defencePower: {$this->defencePower}";
            echo "<br>";
            $this->atraksi();
        }

        abstract public function getInfoHewan();
    }

    class Elang extends Fight{
        public function __construct($string){
            $this->nama = $string;
            $this->jumlahKaki = 2;
            $this->keahlian = "terbang tinggi";
            $this->attackPower = 10;
            $this->defencePower = 5;
        }
        public function getInfoHewan(){
            echo "Jenis Hewan: Elang";
            $this->getInfo();
        }
    }

    class Harimau extends Fight{
        public function __construct($string){
            $this->nama = $string;
            $this->jumlahKaki = 4;
            $this->keahlian = "lari cepat";
            $this->attackPower = 7;
            $this->defencePower = 8;
        }
        public function getInfoHewan(){
            echo "Jenis Hewan: Harimau";
            $this->getInfo();
        }
    }

    class newLine{
        public static function breakTwo(){
            echo"<br><br>";
        }
        public static function breakOne(){
            echo"<br>";
        }
    }

    $harimau = new Harimau("Harimau");
    $harimau->getInfoHewan();

    newLine::breakTwo();

    $elang = new Elang("Elang");
    $elang->getInfoHewan();

    newLine::breakOne();
    $harimau->serang($elang);
    newLine::breakOne();
    $elang->getInfoHewan();

    newLine::breakOne();
    $elang->serang($harimau);
    newLine::breakOne();
    $harimau->getInfoHewan();

?>