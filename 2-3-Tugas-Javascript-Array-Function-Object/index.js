//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();

daftarHewan.forEach((e) => {
    console.log(e);
});

console.log('\n');

//soal 2
function introduce(objectData) {
    return `Nama saya ${objectData.name}, umur saya ${objectData.age} tahun, alamat saya di ${objectData.address}, dan saya punya hobby yaitu ${objectData.hobby}`
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

console.log('\n');

//soal 3
function hitung_huruf_vokal(str){
    vokal = ['a', 'b', 'c', 'd', 'e', 'A', 'B', 'C', 'D', 'E'];
    total = 0;
    for (let i = 0; i < str.length; i++) {
        if(vokal.includes(str[i])){
            total += 1;
        }
    }
    return total;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

console.log('\n');

//soal 4
function hitung(number){
    return (2*number) - 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8