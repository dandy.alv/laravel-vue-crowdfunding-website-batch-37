//jawaban soal 1
const hitungLuas = (a,b) => {
   console.log(`Luas persegi panjang: ${a*b}`)
}

const hitungKeliling = (a,b) => {
   console.log(`Keliling persegi panjang: ${(a+b)* 2}`)
}

hitungLuas(2,4); //8
hitungKeliling(2,4); //12


console.log('\n');

//jawaban soal 2
const newFunction = (firstName, lastName) => {
    return{
        firstName,
        lastName,
        fullName : () => {
            console.log(`${firstName} ${lastName}`);
        }
    }
}
   
newFunction("William", "Imoh").fullName() 


console.log('\n');

//jawaban soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)


console.log('\n');

//jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


console.log('\n');

//jawaban soal 5
const planet = "earth" 
const view = "glass" 
var after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`
console.log(after)
