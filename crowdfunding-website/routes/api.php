<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController');
    Route::post('verification', 'UserVerificationController');
    Route::post('login', 'LoginController');
    Route::post('update-password', 'UpdatePasswordController');
});

Route::group([
    'middleware' => ['email_verification']
], function(){
    Route::get('profile/show', 'ProfileController@show');
    Route::post('profile/update', 'ProfileController@update');
});
