<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/route-1', function(){
//     return 'masuk ke route 1 email sudah terverifikasi';
// })->middleware('email_verification');

// Route::get('/route-2', function(){
//     return 'masuk ke route 2, email sudah terverifikasi dan Anda telah login sebagai admin';
// })->middleware(['email_verification', 'admin_authorization']);
