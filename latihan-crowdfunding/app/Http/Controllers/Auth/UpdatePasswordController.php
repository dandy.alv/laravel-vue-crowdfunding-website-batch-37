<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required',
        ]);

        $email = User::where('email', $request->email)->first();

        if(!$email){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email tidak ditemukan',
            ],400);
        }

        if($request->password !== $request->password_confirmation){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Password yang Anda inputkan tidak cocok.',
            ],400);
        }else{
            //Update Password
            $email->password = Hash::make($request->password);
            $email->save();

            return response()->json([
                'response_code' => '00',
                'response_message' => 'Password berhasil diperbarui.',
            ],200);
        }
    }
}
