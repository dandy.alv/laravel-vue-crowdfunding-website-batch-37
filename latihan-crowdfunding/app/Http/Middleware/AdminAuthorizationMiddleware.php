<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuthorizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if($user->role_id == '84acf413-189e-4fd6-868b-295e62be0132'){
            return $next($request);
        }

        return response()->json([
            'message' => 'Gagal membuka halaman, Anda tidak login sebagai admin'
        ]);
    }
}
