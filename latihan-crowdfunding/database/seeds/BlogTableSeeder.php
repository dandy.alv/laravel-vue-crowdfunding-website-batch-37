<?php

use Illuminate\Database\Seeder;
use App\Blog;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Blog::create([
            'title' => '7 Cara Menaikkan Tekanan Darah Rendah',
            'description' => 'Assalamualaikum nama saya Rudi, ini cara menaikkan tekanan darah rendah'
        ]);

        Blog::create([
            'title' => 'Fungsi Otak dan Cara Menjaga Kesehatannya',
            'description' => 'Assalamualaikum nama saya Budiman, begini cara menjaga kesehatan otak'
        ]);

        Blog::create([
            'title' => '6 Racun dalam Makanan yang Mematikan',
            'description' => 'Assalamualaikum nama saya Adian, berikut 6 Racun dalam makanan yang mematikan'
        ]);
    }
}
