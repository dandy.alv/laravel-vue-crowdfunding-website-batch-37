<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <p>Selamat Bapak/Ibu {{ $name }} kode OTP berhasil di generate kembali.</p>
    <p>Kode OTP untuk memverifikasi akun Anda adalah <b>{{ $otp }}</b></p>
    <small><span style="color: red">*</span> Kode OTP berlaku hingga 5 menit dari sekarang.</small>

</body>
</html>